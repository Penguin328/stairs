<?php

return [
    // The number of minutes that users have to wait between tracking exercise
    'waitMinutes' => 5,
];
