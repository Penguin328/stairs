@extends('app')

@section('content')
    <div class="container">
        <div class="page-header">
            <h1>{{ $userName }}
                <small>Score: {{ $exercises->count() }}</small>
            </h1>
        </div>

        <table class="table table-striped">
            <thead>
                <th>Location</th>
                <th>Logged At</th>
            </thead>
            <tbody>
                @foreach ($exercises as $exercise)
                    <tr class="{{ $exercise->pivot->is_cheated ? 'danger' : null }}">
                        <td>{{ $exercise->name }}</td>
                        <td>{{ $exercise->pivot->created_at->format('D, j M g:i A') }}</td>
                    </tr>
                @endforeach
                @if ($exercises->isEmpty())
                    <tr>
                        <td class="warning" colspan="2">No exercise has been logged.</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
@endsection
