@extends('app')

@section('content')
    <div class="container">
        @include('partials.messages')

        <div class="page-header">
            <h1>High Scores</h1>
        </div>

        <div class="row well">
            <div class="col-xs-4">
                <strong>{{ number_format($totalScans) }}</strong> Scans
            </div>
            <div class="col-xs-4">
                <strong>{{ number_format($totalScans * 4) }}</strong> Floors
            </div>
            <div class="col-xs-4">
                <strong>{{ number_format($totalScans * 4 * 24) }}</strong> Steps
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <th>Score</th>
                    <th>Floors</th>
                    <th>Steps</th>
                    <th>Name</th>
                    <th>Last Exercise</th>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr class="{{ ($user->id == Auth::user()->id) ? 'info' : null }}"
                            data-user-id="{{ $user->id }}" style="cursor: pointer">
                            <td>
                                {{ number_format($user->score) }}
                                @if ($user->cheatingScore > 0)
                                    <em class="text-danger">({{ $user->cheatingScore }})</em>
                                @endif
                            </td>
                            <td>{{ number_format($user->score * 4) }}</td>
                            <td>{{ number_format($user->score * 4 * 24) }}</td>
                            <td>{{ $user->name }}</td>
                            <td>
                                {!! $user->lastExerciseDate ? $user->lastExerciseDate->format('D, j M g:i A') : '<em>Never</em>' !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('table tbody tr').click(function () {
            window.location.href = "/user/" + $(this).data('userId');
        });
    </script>
@endsection
