@extends('app')

@section('content')
    <div class="container">
        @include('partials.messages')

        <div class="page-header">
            <h1>Exercises</h1>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <th>ID</th>
                    <th>QR</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Last Modified</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                    @foreach ($exercises as $exercise)
                        <tr>
                            <td>{{ $exercise->id }}</td>
                            <td>
                                <a href="{{ route('exercise.show', ['id' => $exercise->id]) }}" title="Click to view larger">
                                    <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(25)->generate(action('ExerciseController@getTrack', ['token' => $exercise->token]))) !!} ">
                                </a>
                            </td>
                            <td>{{ $exercise->name }}</td>
                            <td>{{ $exercise->is_active ? 'Active' : 'Inactive' }}</td>
                            <td>{{ $exercise->updated_at }}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-xs btn-info" href="{{ action('ExerciseController@edit', ['id' => $exercise->id]) }}"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a class="btn btn-xs btn-danger" href="{{ action('ExerciseController@getDestroy', ['id' => $exercise->id]) }}"><i class="glyphicon glyphicon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @if ($exercises->count() === 0)
                        <tr>
                            <td colspan="7" class="warning">No exercises found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>

        <a class="btn btn-primary" href="{{ action('ExerciseController@create') }}">
            <i class="glyphicon glyphicon-plus"></i> New
        </a>
    </div>
@endsection
