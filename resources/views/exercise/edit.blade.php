@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create Exercise</div>
                    <div class="panel-body">
                        @include('errors.input')

                        {!! Form::model($exercise, ['method' => 'PUT', 'action' => ['ExerciseController@update', $exercise->id], 'class' => 'form-horizontal']) !!}
                            @include('exercise.partials.form', ['submitButtonText' => 'Update Exercise'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
