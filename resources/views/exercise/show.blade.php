@extends('app')

@section('content')
    <div class="container">
        @include('partials.messages')

        <div class="page-header">
            <h1>{{ $exercise->name }}</h1>
        </div>

        <div>
            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(300)->generate(action('ExerciseController@getTrack', ['token' => $exercise->token]))) !!} ">
        </div>
    </div>
@endsection
