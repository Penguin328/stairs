<div class="form-group">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}

    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('is_active', 'Status', ['class' => 'col-md-4 control-label']) !!}

    <div class="col-md-6">
        {!! Form::select('is_active', ['1' => 'Active', '0' => 'Inactive'], null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('token', 'Tokens', ['class' => 'col-md-4 control-label']) !!}

    <div class="col-md-6">
        {!! Form::textarea('token', null, ['class' => 'form-control']) !!}
        <p class="help-block">
            <button type="button" id="generate-token" class="btn btn-link">Generate unique token</button>
        </p>
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            <i class="glyphicon glyphicon-floppy-disk"></i> {{ $submitButtonText }}
        </button>

        <a class="btn btn-link" href="{{ url('exercise') }}">Cancel</a>
    </div>
</div>

@section('scripts')
    <script>
        $('#generate-token').click(function() {
            $.get('{{ url('uuid/generate') }}').done(function(uuid) {
                var $token = $('#token'),
                    existingTokensRaw = $token.val().trim(),
                    existingTokens = [];

                if(existingTokensRaw.length) {
                    existingTokens = existingTokensRaw.split('\n');
                }

                existingTokens.push(uuid);

                $token.val(existingTokens.join('\n'));
            });
        });
    </script>
@endsection
