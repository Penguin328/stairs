<?php

use App\Exercise;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        // Deleting all users will delete all exercises
        DB::table('users')->delete();
        DB::table('exercises')->delete();
        DB::table('exercise_user')->delete();

        $user = User::create([
            'name' => 'Test User',
            'email' => 'chris.olszewski@assistrx.com',
            'password' => Hash::make('password')
        ]);

        $exercise = Exercise::create([
            'name' => 'East Stairs',
            'token' => Uuid::generate(4),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        foreach (['-1 day', '-3 hours', '-5 minutes'] as $timeAgo) {
            $date = date('Y-m-d H:i:s', strtotime($timeAgo));

            $user->exercises()->attach([$exercise->id => ['created_at' => $date, 'updated_at' => $date]]);
        }
    }
}
