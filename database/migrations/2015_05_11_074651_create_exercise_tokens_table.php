<?php

use Stairs\Exercise;
use Stairs\ExerciseToken;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseTokensTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exercise_id')->unsigned();
            $table->foreign('exercise_id')->references('id')->on('exercises')->onDelete('cascade');
            $table->string('token');
			$table->timestamps();
            $table->softDeletes();
		});

        Exercise::withTrashed()->get()->each(function(Exercise $exercise) {
            ExerciseToken::create([
                'exercise_id' => $exercise->id,
                'token' => $exercise->token
            ]);
        });

        if(Schema::hasColumn('exercises', 'token')) {
            Schema::table('exercises', function (Blueprint $table) {
                $table->dropColumn('token');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(!Schema::hasColumn('exercises', 'token')) {
            Schema::table('exercises', function (Blueprint $table) {
                $table->string('token')->after('name');
            });
        }

        ExerciseToken::withTrashed()->get()->each(function(ExerciseToken $exerciseToken) {
            $exercise = Exercise::find($exerciseToken->exercise_id);

            if($exercise !== null) {
                $exercise->token = $exerciseToken->token;
                $exercise->save();
            }
        });

        Schema::drop('exercise_tokens');
    }

}
