<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExerciseActiveColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('exercises', function ($table) {
			$table->boolean('is_active')->default(true);
		});

		Schema::table('exercise_user', function ($table) {
			$table->boolean('is_cheated')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('exercises', function ($table) {
			$table->dropColumn('is_active');
		});

		Schema::table('exercise_user', function ($table) {
			$table->dropColumn('is_cheated');
		});
	}

}
