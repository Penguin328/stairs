cd into the project root and run the following commands:


```
#!bash

composer update
touch storage/database.sqlite
chmod -R 777 storage
cp .env.example .env
php artisan migrate
```