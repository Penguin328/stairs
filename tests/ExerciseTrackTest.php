<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use Stairs\Commands\TrackExerciseCommand;

class ExerciseTrackTest extends TestCase
{
    use DatabaseMigrations;

    public function testInvalidToken()
    {
        $user = factory('Stairs\User')->create();

        $this->setExpectedException('Stairs\Exceptions\ExerciseTrackingException');
        Bus::dispatch(new TrackExerciseCommand($user, 'abc'));
    }

    public function testValidToken()
    {
        $user = factory('Stairs\User')->create();

        $exercise = factory('Stairs\Exercise')->create();

        $faker = \Faker\Factory::create();

        $exerciseToken = Stairs\ExerciseToken::create([
            'exercise_id' => $exercise->id,
            'token' => $faker->uuid
        ]);

        $exercise->tokens()->save($exerciseToken);

        Bus::dispatch(new TrackExerciseCommand($user, $exerciseToken->token));

        $this->seeInDatabase('exercise_user', [
            'exercise_id' => $exercise->id,
            'user_id' => $user->id,
            'is_cheated' => false
        ]);
    }

    public function testMultipleValidToken()
    {
        $user = factory('Stairs\User')->create();

        $exercise = factory('Stairs\Exercise')->create();

        $faker = \Faker\Factory::create();

        $exerciseToken = Stairs\ExerciseToken::create([
            'exercise_id' => $exercise->id,
            'token' => $faker->uuid
        ]);

        $exercise->tokens()->save($exerciseToken);

        Bus::dispatch(new TrackExerciseCommand($user, $exerciseToken->token));

        DB::table('exercise_user')->update(['created_at' => date('Y-m-d H:i:s', strtotime('-10 minutes'))]);

        Bus::dispatch(new TrackExerciseCommand($user, $exerciseToken->token));

        // Assert that no exceptions are thrown from the second exercise tracking
        $this->assertTrue(true);
    }

    public function testCheatCatching()
    {
        $user = factory('Stairs\User')->create();

        $exercise = factory('Stairs\Exercise')->create();

        $faker = \Faker\Factory::create();

        $exerciseToken = Stairs\ExerciseToken::create([
            'exercise_id' => $exercise->id,
            'token' => $faker->uuid
        ]);

        $exercise->tokens()->save($exerciseToken);

        $exerciseToken->delete();

        Mail::shouldReceive('raw')->once()->with($user->name . ' - ' . $exercise->name, Mockery::on(function($closure) {
            $message = Mockery::mock('Illuminate\Mail\Message');
            $message->shouldReceive('from')->with('admin@stairs.kristoff.me', 'AssistRx Wellness');
            $message->shouldReceive('to')->with('chris.olszewski@assistrx.com');
            $message->shouldReceive('subject')->with('User Cheated');

            $closure($message);
            return true;
        }));

        $this->setExpectedException('Stairs\Exceptions\ExerciseTrackingException');
        Bus::dispatch(new TrackExerciseCommand($user, $exerciseToken->token));
    }

    public function testCheatDelay()
    {
        $user = factory('Stairs\User')->create();

        $exercise = factory('Stairs\Exercise')->create();

        $faker = \Faker\Factory::create();

        $exerciseToken = Stairs\ExerciseToken::create([
            'exercise_id' => $exercise->id,
            'token' => $faker->uuid
        ]);

        $exercise->tokens()->save($exerciseToken);

        $exerciseToken->delete();

        Mail::shouldReceive('raw')->once()->with($user->name . ' - ' . $exercise->name, Mockery::on(function($closure) {
            $message = Mockery::mock('Illuminate\Mail\Message');
            $message->shouldReceive('from')->with('admin@stairs.kristoff.me', 'AssistRx Wellness');
            $message->shouldReceive('to')->with('chris.olszewski@assistrx.com');
            $message->shouldReceive('subject')->with('User Cheated');

            $closure($message);
            return true;
        }));

        // Tracking this exercise should raise an exception, but we need to check for it this way so that we can
        // continue the test to the next step
        $exceptionRaised = false;

        try {
            Bus::dispatch(new TrackExerciseCommand($user, $exerciseToken->token));
        } catch(Stairs\Exceptions\ExerciseTrackingException $e) {
            $exceptionRaised = true;
        }

        if(!$exceptionRaised) {
            $this->fail('Exception was not raised');
        }

        // Attempt to track a new exercise with a valid token
        $exerciseToken = Stairs\ExerciseToken::create([
            'exercise_id' => $exercise->id,
            'token' => $faker->uuid
        ]);

        $exercise->tokens()->save($exerciseToken);

        $this->setExpectedException('Stairs\Exceptions\ExerciseTrackingException');
        Bus::dispatch(new TrackExerciseCommand($user, $exerciseToken->token));
    }

}
