<?php namespace Stairs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exercise extends Model
{

    /**
     * So that we can still reference these in our pivot table even after they are deleted
     */
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'is_active'];

    /**
     * Indicate that these attributes are date types
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function users()
    {
        return $this->belongsToMany('Stairs\User')->withTimestamps();
    }

    public function tokens()
    {
        return $this->hasMany('Stairs\ExerciseToken');
    }

}
