<?php namespace Stairs\Http\Controllers;

use Stairs\Commands\TrackExerciseCommand;
use Stairs\Exceptions\ExerciseTrackingException;
use Stairs\Exercise;
use Stairs\ExerciseToken;
use Stairs\Http\Requests;
use Stairs\Http\Requests\ExerciseRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExerciseController extends Controller
{

    /**
     * Require users log in before accessing any of these methods
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display all of the active exercises.
     *
     * @return Response
     */
    public function index()
    {
        $exercises = Exercise::all();

        return view('exercise.index', compact('exercises'));
    }

    public function show($id)
    {
        $exercise = Exercise::find($id);

        return view('exercise.show', compact('exercise'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('exercise.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(ExerciseRequest $request)
    {
        $exercise = Exercise::create($request->except(['token']));

        $tokens = explode("\n", $request->get('token'));
        foreach($tokens as $token) {
            $token = trim($token);

            $tokenCount = ExerciseToken::where('token', '=', $token)->count;
            if($tokenCount > 0) {
                continue;
            }

            ExerciseToken::create(['exercise_id' => $exercise->id, 'token' => $token]);
        }

        return redirect('exercise')->with('message', 'Exercise created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $exercise = Exercise::with('tokens')->findOrFail($id);

        $exercise->token = $exercise->tokens->lists('token')->implode("\n");

        return view('exercise.edit', compact('exercise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ExerciseRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(ExerciseRequest $request, $id)
    {
        $exercise = Exercise::findOrFail($id);
        $exercise->update($request->except(['token']));

        $existingTokens = $exercise->tokens->lists('token')->toArray();

        // Tokens are passed in one-per-line
        $tokens = explode("\n", $request->get('token'));
        $tokens = array_map('trim', $tokens);

        // Add any new tokens and ensure that no other exercises use them
        foreach(array_diff($tokens, $existingTokens) as $token) {
            $tokenCount = ExerciseToken::where('token', '=', $token)->count();
            if($tokenCount > 0) {
                continue;
            }

            ExerciseToken::create(['exercise_id' => $exercise->id, 'token' => $token]);
        }

        // Delete existing tokens that are no longer present
        foreach(array_diff($existingTokens, $tokens) as $token) {
            ExerciseToken::where('token', '=', $token)->delete();
        }

        return redirect('exercise')->with('message', 'Exercise updated successfully!');
    }

    /**
     * Soft-delete the exercise.
     * Using getDestroy instead of destroy so that we can reference this from a link and not require a form or AJAX call
     *
     * @param  int $id
     * @return Response
     */
    public function getDestroy($id)
    {
        Exercise::destroy($id);

        return redirect('exercise')->with('message', 'Exercise deleted successfully!');
    }

    /**
     * Track a user performing a given exercise.
     * This SHOULD only be reached after scanning a QR code.
     *
     * @param string $token The tracking token for a given exercise
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getTrack($token)
    {
        try {
            $this->dispatch(new TrackExerciseCommand(Auth::user(), $token));
        } catch(ExerciseTrackingException $e) {
            return redirect()->action('UserController@getHighScores')->with('error', $e->getMessage());
        }

        return redirect()->action('UserController@getHighScores')->with('message', 'Exercise logged successfully!');
    }

}
