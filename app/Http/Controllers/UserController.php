<?php namespace Stairs\Http\Controllers;

use Stairs\Http\Requests;
use Stairs\User;
use Auth;
use DB;

class UserController extends Controller
{

    /**
     * Require users log in before accessing any of these methods
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id = null)
    {
        $user = is_null($id) ? Auth::user() : User::find($id);

        $exercises = $user->exercises()->withPivot('is_cheated')->orderBy('exercise_user.created_at', 'desc')->get();

        return view('user.profile')->with(['userName' => $user->name, 'exercises' => $exercises]);
    }

    public function getHighScores()
    {
        $users = User::all()
            ->each(function(User $user) {
                $totalScore = $user->exercises()->withTrashed()->count();
                $cheatingScore = $user->exercises()->withTrashed()->where('is_cheated', 1)->count();

                $user->score = $totalScore - $cheatingScore;
                $user->cheatingScore = $cheatingScore;

                $lastExercise = $user->lastExercise();
                $user->lastExerciseDate = is_null($lastExercise) ? null : $lastExercise->pivot->created_at;
            })->sortByDesc(function($user) {
                return $user->score;
            });

        $totalScans = DB::table('exercise_user')->where('is_cheated', '=', 0)->count();

        return view('user.highScores')->with(['users' => $users, 'totalScans' => $totalScans]);
    }

}
