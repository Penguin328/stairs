<?php namespace Stairs\Http\Requests;

/**
 * By abstracting this here, we can keep our controller clean and DRY
 *
 * Class ExerciseRequest
 * @package Stairs\Http\Requests
 */
class ExerciseRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // For now we do not have any special rules on who can manage exercises, but in the future we could
        // define those here
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255',
            'token' => 'required',
            'is_active' => 'required|boolean',
        ];

        return $rules;
    }

}
