<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function() {
    return redirect()->action('UserController@getHighScores');
});

Route::get('uuid/generate', function() {
    if (!Auth::check()) {
        abort(403, 'Unauthorized action.');
    }

    echo Uuid::generate(4);
});

Route::get('/user/{id?}', ['uses' => 'UserController@index'])->where('id', '[0-9]+');

Route::resource('exercise', 'ExerciseController');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
    'exercise' => 'ExerciseController',
    'user' => 'UserController'
]);
