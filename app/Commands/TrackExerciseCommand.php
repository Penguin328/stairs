<?php namespace Stairs\Commands;

use Illuminate\Mail\Message;
use Stairs\Exceptions\ExerciseTrackingException;
use Stairs\Exercise;
use Stairs\ExerciseToken;
use Stairs\User;
use Config;
use Illuminate\Contracts\Bus\SelfHandling;
use Log;
use Mail;

class TrackExerciseCommand extends Command implements SelfHandling {

    protected $user, $token;

    /**
     * Create a new command instance.
     *
     * @param User $user
     * @param string $token
     */
	public function __construct(User $user, $token)
	{
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Execute the command.
     *
     * @throws ExerciseTrackingException
     */
	public function handle()
	{
        $exerciseToken = ExerciseToken::where('token', '=', $this->token)->withTrashed()->first();
        if(is_null($exerciseToken)) {
            Log::info(
                'User attempted to track exercise with an invalid token.',
                ['user_id' => $this->user->id, 'token' => $this->token]
            );

            throw new ExerciseTrackingException('Invalid exercise token.');
        }

        $exercise = $exerciseToken->exercise;
        
        $lastExercise = $this->user->lastExercise();
        if($lastExercise !== null) {
            $waitMinutes = Config::get('exercise.waitMinutes');

            $lastExerciseTime = strtotime($lastExercise->pivot->created_at);

            $lastCheatedExercise = $this->user->exercises()
                ->where('is_cheated', '=', 1)
                ->orderBy('exercise_user.created_at', 'desc')
                ->first();
            if($lastCheatedExercise !== null) {
                $lastExerciseTime = strtotime($lastCheatedExercise->pivot->created_at);
                $waitMinutes = 120;
            }

            $cutoffTime = strtotime("-$waitMinutes minutes");
            if($cutoffTime < $lastExerciseTime) {
                Log::info(
                    'User attempted to track exercise before wait period expired.',
                    ['user_id' => $this->user->id, 'exercise_id' => $exercise->id]
                );

                throw new ExerciseTrackingException("You must wait at least $waitMinutes minutes between tracking exercise.");
            }
        }

        if(!is_null($exerciseToken->deleted_at) || !is_null($exercise->deleted_at) || !$exercise->is_active) {
            Log::info(
                'User logged on an inactive exercise.',
                ['user_id' => $this->user->id, 'exercise_id' => $exercise->id]
            );

            Mail::raw($this->user->name . ' - ' . $exercise->name, function(Message $message) {
                $message->from('admin@stairs.kristoff.me', 'AssistRx Wellness');
                $message->to('chris.olszewski@assistrx.com');
                $message->subject('User Cheated');
            });

            $this->user->exercises()->attach($exercise->id, ['is_cheated' => true]);

            throw new ExerciseTrackingException("You have attempted to log exercise on an inactive token.");
        }

        $this->user->exercises()->attach($exercise->id);
	}

}
