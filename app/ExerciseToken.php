<?php namespace Stairs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExerciseToken extends Model
{
    /**
     * So that we can tell an invalid token from a cheating token
     */
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['exercise_id', 'token'];

    /**
     * Indicate that these attributes are date types
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function exercise()
    {
        return $this->belongsTo('Stairs\Exercise');
    }
}
